# The question is "Why does Proc.new work here without a block, yet understands
# the block given to the method"
def teach_trick method_name, &blk
  block_given? && define_singleton_method(method_name, blk)
end

teach_trick( 'hello') { puts 'hello world' }
hello

# Explicitly asking for a block in the argument list is better anyway, the
# method signature becomes clear and this works with no problems at all.
